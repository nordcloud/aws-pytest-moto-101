# Testing in Software Development

## Why Test Automation

1. **Incremental development**: The first and foremost factor that necessitates automation in agile testing is the short development cycle. Agile teams have only a few weeks to get a grasp of the requirement, make the code changes and test the changes. If all testing were to be done manually, the time required would surpass the actual development time. Alternatively, testing would have to be hurried, thus compromising on quality.
2. **Frequent Changes**: Agile projects do not work with a complete set of requirements. The requirements develop with time and often change depending on customer priorities, market trends and end-user needs. While the most positive trait of the agile method is its quick adaptability to change, it also implies testing has to be agile enough to meet the changes. Automation brings in necessary agility to testing and helps it to respond faster and more effectively to changes.
3. **Continuous testing**: Agility demands early and continuous testing. Test coverage extends to not only the newly added code but also the code from previous iterations. This is to ensure previous functionality is not broken due to the newly added functionality. This puts a lot of pressure on the testers and can seriously affect the quality of the product. Automating some of the testing means testers have more time in hand for exploratory testing.
   Get quick visibility into code quality: Automation testing allows to quickly test the code with a standard suite of test scripts. This gives the tester and developer a quick peek into the code quality and they have more time to react in case the code is not up to expectations.
4. **Automating test support activities**: Automation in testing is not only for executing test scripts against the code but it can also be used to automate other testing activities like data set up, test result validation and test reporting. Agility requires frequent code deployments, which can also be automated. This frees testers from mundane, repetitive tasks so that they can focus more on testing.
5. **Exhaustive testing**: With automation, testing can be repeated as many times allowing detailed and exhaustive examination of the code. This is highly useful in ensuring code quality when working in a limited testing window.

**Test automation in agile projects is best developed incrementally and it should start parallel to development so that time that can be utilized for testing is not wasted in enabling automation. Test automation should be a well-thought-out process for it to be cost effective and generate ample returns.**

_Source:_ <https://www.cigniti.com/blog/6-reasons-why-automation-is-necessary-in-agile-testing/>

---

## Our Case Study

### Python

**Why Pystest?**

Pytest is an open-source testing framework that is possibly on of the most widely used Python testing frameworks out there. Pytest is supports unit testing, functional testing and API testing as well. To run it you will need Python version 3.5 or higher.

#### Pros

- Allows for compact and simple test suites.
- Is highly extensible by using plugins, such as: pytest-randomly, pytest-cov, pytest-django, pytest-bdd.
- You can also add pytest html plugin to your project to print HTML reports with one simple command-line option.
- Can run tests in parallel using a Pytest plugin pytest-xdist. You can read more about it here too.
- Has a very large community.
- Supports fixtures helping you to cover all the parameter combinations without rewriting test cases, and are a great way to manage context between steps.

#### Cons

- Compatibility is not key for Pytest, as although you can easily write test cases with Pytest, you won’t be able to use those within any other testing framework due to the usage of Pytest’s unique routines.

**Bottom line:** If you are looking to create, unit tests, small and concise tests, that support complex scenarios, this mature full-featured framework is for you.

_Source:_ <https://blog.testproject.io/2020/10/27/top-python-testing-frameworks/>

### AWS/boto3 --> moto

**Why moto?**

Our interaction with the AWS infrastructure is implemented with boto3, the AWS SDK for Python.

Moto is a good fit in such a case, because it enables you to mock out all calls to AWS made in your code automatically.
There is no need for dependency injection. You can take the code you have in place and test it straight away. You just need to enable motos mocking functionality and take some precautions, that is it.

_Source:_ <https://blog.codecentric.de/en/2020/01/testing-aws-python-code-with-moto/>

---

### Pytest

<https://docs.pytest.org/en/stable/>

#### Fixture

<https://docs.pytest.org/en/stable/fixture.html#fixtures>

#### Test Discovery

<https://docs.pytest.org/en/stable/goodpractices.html#test-discovery>

### moto

<https://github.com/spulec/moto>

<http://docs.getmoto.org/en/latest/docs/getting_started.html>

#### Pytest and moto comes together

<https://github.com/spulec/moto#example-on-usage>

<https://www.learnaws.org/2020/12/01/test-aws-code/>
