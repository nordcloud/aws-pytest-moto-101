# Development Setup

## Python Development Setup

Be sure that you are running on Python Version 3.10, eventually check:

- <https://pypi.org/project/pyenv-win/#installation> for Windows
- <https://github.com/pyenv/pyenv> for Linux/Mac

make sure that you had install pip

```bash
pip install -U pip
```

Create a new python enviroment and activate it:

1. On Mac/Linux use pyenv-virtualenv (<https://github.com/pyenv/pyenv-virtualenv>)

```bash
pyenv virtualenv 3.10.2 pytest-moto-101
pyenv activate pytest-moto-101
cd <project>
pip install -r requirements-dev.txt
```

2. On Windows using virtualenv and virtualenvwrapper (<https://github.com/davidmarble/virtualenvwrapper-win/>)

```bash
pip install virtualenv
pip install virtualenvwrapper-win
cd <project>
mkvirtualenv -p3.10 pytest-moto-101
%USERPROFILE%/Envs/pytest-moto-101/Scripts/activate.bat
pip install -r requirements-dev.txt
```

---

## IDE Setup

### VS Code

Install Python extension:
<https://marketplace.visualstudio.com/items?itemName=ms-python.python>

Select the created environment:
<https://code.visualstudio.com/docs/python/environments#_select-and-activate-an-environment>

Install Test Explorer UI extension:
<https://marketplace.visualstudio.com/items?itemName=hbenl.vscode-test-explorer>

Install Python Test Explorer extension:
<https://marketplace.visualstudio.com/items?itemName=littlefoxteam.vscode-python-test-adapter>

Additional info:
<https://code.visualstudio.com/docs/python/testing>

Setup VS Code .vscode/settings to instruct VS Code to use Pytest:

```json
{
  "python.testing.pytestArgs": [],
  "python.testing.unittestEnabled": false,
  "python.testing.pytestEnabled": true
}
```

Or click on __View__ -> __Command Palette...__

search for __Python: Configure Tests__

select __pytest__

select __Use existing config file <sub><sup>setup.cfg<sub><sup>__

### PyCharm

Check the configuration at:
<https://www.jetbrains.com/help/pycharm/pytest.html>

## Command line usage

<https://docs.pytest.org/en/stable/usage.html#cmdline>
