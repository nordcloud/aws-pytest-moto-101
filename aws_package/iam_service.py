from __future__ import annotations

import logging
from typing import Any, Dict, List

from boto3 import Session
from botocore.client import BaseClient

from aws_package.base_service import AwsService

LOG = logging.getLogger()


class IamService(AwsService):
    '''
    Implementation of the AWS IAM service
    (wrapper around boto3.session.Session.client('iam'))

    Reference:
    https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html

    Args:
        boto_iam_client (object): the instance of a
            boto3.session.Session.client('iam')
    '''

    def __init__(self, boto_iam_client: BaseClient):
        super().__init__(boto_iam_client)

    @classmethod
    def from_session(cls, session: Session, region_name: str) -> IamService:
        return super().from_session(session, 'iam', region_name)

    def get_all_users(self) -> List[Dict[str, Any]]:
        '''
        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.list_users
        '''
        users = []
        response = self._client.list_users()
        users.extend(response.get('Users', []))

        while response.get('IsTruncated', False):
            response = self._client.list_users(Marker=response.get('Marker', None))
            users.extend(response.get('Users', []))

        LOG.debug('List of User=%s', users)
        return users

    def _get_all_access_keys_by_user(self, user_name: str) -> List[Dict[str, Any]]:
        '''
        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.list_access_keys
        '''
        access_keys = []
        response = self._client.list_access_keys()
        access_keys.extend(response.get('AccessKeyMetadata', []))

        while response.get('IsTruncated', False):
            response = self._client.list_access_keys(Marker=response.get('Marker', None))
            access_keys.extend(response.get('AccessKeyMetadata', []))

        LOG.debug('List of Keys for user=%s: %s', user_name, access_keys)
        return access_keys

    def get_all_users_access_keys(self) -> List[Dict[str, Any]]:
        '''
        Get all the IAM users and the access keys associated with it
        '''
        users_access_keys = []
        users = self.get_all_users()
        for user in users:
            users_access_keys.append(
                {
                    'UserName': user['UserName'],
                    'UserId': user['UserId'],
                    'Arn': user['Arn'],
                    'AccessKeys': self._get_all_access_keys_by_user(user['UserName'])
                },)
        return users_access_keys
        # Using collation
        # return [{
        #     'UserName': user['UserName'],
        #     'UserId': user['UserId'],
        #     'Arn': user['Arn'],
        #     'AccessKeys': self.get_all_access_keys_by_user(user['UserName'])
        # } for user in self.get_all_users()]

    def delete_user(self, user_name: str):
        '''
        Delete the specified IAM user, delete the user permissions boundary and
        detach all the policies attached to it, without delete those policies

        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.list_attached_user_policies
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.detach_user_policy
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.list_user_policies
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.delete_user_policy
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html#IAM.Client.delete_user

        Args:
            role_name (str): The name of the role to delete
        '''
        attached_policies = []
        response = self._client.list_attached_user_policies(UserName=user_name)
        attached_policies.extend(response.get('AttachedPolicies', []))

        while response.get('IsTruncated', False):
            response = self._client.list_attached_user_policies(UserName=user_name,
                                                                Marker=response.get('Marker', None))
            attached_policies.extend(response.get('AttachedPolicies', []))

        [
            self._client.detach_user_policy(UserName=user_name, PolicyArn=attached_policy['PolicyArn'])
            for attached_policy in attached_policies
        ]

        inline_policies = []
        response = self._client.list_user_policies(UserName=user_name)
        inline_policies.extend(response.get('PolicyNames', []))

        while response.get('IsTruncated', False):
            response = self._client.list_user_policies(UserName=user_name, Marker=response.get('Marker', None))
            inline_policies.extend(response.get('PolicyNames', []))

        [
            self._client.delete_user_policy(UserName=user_name, PolicyName=inline_policy)
            for inline_policy in inline_policies
        ]

        self._client.delete_user(UserName=user_name)
