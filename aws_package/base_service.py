from __future__ import annotations

from boto3 import Session


class AwsService:
    """
    Class represent a generic AWS Service that use a
    boto3.session.Session.client to perform actions on AWS

    Reference:
    https://boto3.amazonaws.com/v1/documentation/api/latest/reference/core/session.html#boto3.session.Session.client

    Args:
        boto3_client (object): the instance of a boto3.session.Session.client
    """

    def __init__(self, boto3_client: object):
        self._client = boto3_client

    @property
    def region_name(self) -> str:
        """
        Get the region name where we are operating with the instance of the
        class

        Returns:
            (str): the region name of the service
        """
        return self._client.meta.region_name

    @classmethod
    def from_session(cls, session: Session, service: str, region_name: str) -> AwsService:
        """
        Class method used to create an instance of an AwsService starting from
        a defined session

        Args:
            session (Session): the boto3.Session used to create the boto3 client
            service (str): the name of a service, e.g. 's3' or 'ec2'.
                You can get a list of available services via
                get_available_services()
            region_name (str): the name of the region associated with the
                client. A client is associated with a single region.

        Returns:
            (AwsService): the class represent a generic AWS Service
        """
        return cls(session.client(service, region_name))
