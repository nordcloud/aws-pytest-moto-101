from __future__ import annotations

from typing import Union

from boto3 import Session
from botocore.client import BaseClient

from aws_package.base_service import AwsService


class S3Service(AwsService):

    def __init__(self, boto_s3_client: BaseClient):
        super().__init__(boto_s3_client)

    @classmethod
    def from_session(cls, session: Session, region_name: str) -> S3Service:
        return super().from_session(session, 's3', region_name)

    def upload(self, bucket: str, key: str, body: Union[bytes, object]):
        return self._client.put_object(Bucket=bucket, Key=key, Body=body)

    def download(self, bucket: str, key: str):
        return self._client.get_object(Bucket=bucket, Key=key)
