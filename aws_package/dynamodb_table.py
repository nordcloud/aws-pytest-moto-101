import random
import string
import logging
from typing import Any, Dict, List, Tuple, Union

LOG = logging.getLogger()


class DynamoDBTable:
    '''
    Generic class to data access in AWS DynamoDB
    (wrapper around boto3.session.resource('dynamodb').Table(<table>))

    Reference:
    https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#table

    Args:
        boto_resource_dynamodb_table (object): instance of
            boto3.resources.factory.dynamodb.Table which the instance of this
            class point to
    '''

    def __init__(self, boto_resource_dynamodb_table: object):
        self._boto_dynamodb_table = boto_resource_dynamodb_table
        self._table_schema = self._build_table_schema()

    def _build_table_schema(self) -> Dict[str, Union[str, List[Dict[str, Any]]]]:
        '''
        Build the DynamoDB table schema for this class via the boto3 DynamoDB
        Table resource. This method is used, if the the table_schema was not
        provided to the constructor.

        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Table.key_schema
        '''
        table_schema = {'table_name': self._boto_dynamodb_table.table_name, 'range_key': '-'}

        for key in self._boto_dynamodb_table.key_schema:
            if key['KeyType'] == 'HASH':
                table_schema['hash_key'] = key['AttributeName']
            elif key['KeyType'] == 'RANGE':
                table_schema['range_key'] = key['AttributeName']

        return table_schema

    @property
    def table_name(self) -> str:
        '''
        Represent the full table name of the DynamoDB table

        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Table.table_name
        '''
        return self._table_schema['table_name']

    @property
    def hash_key(self) -> str:
        '''
        Represent the field used as hash key for the DynamoDB table

        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Table.key_schema
        '''
        return self._table_schema['hash_key']

    @property
    def range_key(self) -> str:
        '''
        Represent the field used as range key for the DynamoDB table

        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Table.key_schema
        '''
        return self._table_schema['range_key'] if self._table_schema['range_key'] else None

    def _create_key_arg(self, hash_key_value: str, range_key_value: str = None) -> Dict[str, str]:
        '''
        Create the key arg used in the
        boto3.session.resource('dynamodb').Table(<table>).get_item(Key=<key_dict>)

        Args:
            hash_key_value (str): the value of the hash key for the item we
                want to retrieve
            range_key_value (str): the value of the range key for the item
                we want to retrieve, needeed only if range key defined

        Returns:
            key (Dict[str, str]): the dictionary to pass to as key argument

        Raises:
            Exception: the expected key has to be composite, only
                hash_key used
        '''
        key = {self.hash_key: hash_key_value}

        if self.range_key:
            if not range_key_value:
                raise Exception("Exception raised in DynamoDBTable", [
                    f"Table={self.table_name} has a composite key",
                    f"Value for range_key={self.range_key} not specified"
                ])

            key[self.range_key] = range_key_value
        return key

    def get_item_by_key(
        self,
        hash_key_value: str,
        range_key_value: str = None,
        consistent_read: bool = False,
    ) -> Dict[str, Any]:
        '''
        Get one item from the table specifing the value of the key using the
        boto3.session.resource('dynamodb').Table(<table>).get_item() method

        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Table.get_item

        Args:
            hash_key_value (str): the value of the hash key for the item we
                want to retrieve
            range_key_value (str): the value of the range key for the item
                we want to retrieve, needeed only if range key defined
            consistent_read (bool): optional, determines the read consistency
                model

        Returns:
            (Dict[str, Any]): the dictionary that map the item we want to
                retrive
        '''
        LOG.debug("Get item from DynamoDB table %s for key=%s,%s", self.table_name, hash_key_value, range_key_value)
        key = self._create_key_arg(hash_key_value, range_key_value)

        response = self._boto_dynamodb_table.get_item(Key=key, ConsistentRead=consistent_read)
        return response.get('Item', {})

    def add_item(self, item: Dict[str, Any]) -> Dict[str, Any]:
        '''
        Creates a new dynamodb record in the table using the
        boto3.session.resource('dynamodb').Table(<table>).put_item() method

        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Table.put_item

        Args:
            item (Dict[str, Any]): the item to add into the table

        Returns:
            item (Dict[str, Any]): the dictionary represent the item just
                inserted
        '''
        response = self._boto_dynamodb_table.put_item(Item=item)
        LOG.debug("Created new DynamoDB item response=%s", response)

        return item

    @staticmethod
    def _create_random_expression_attribute_name_key(field: str) -> str:
        '''
        Create a random string with a '#' prefix in order to be used as key of
        the ExpressionAttributeNames dictionary passed to the scan or query
        method in order to avoid any possible reserved words

        Reference:
        https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/ReservedWords.html

        Args:
            field (str): the field we want to create the key for
                ExpressionAttributeNames

        Returns:
            (str): the random string with the '#' prefix, it has the same
                length of the field passed as argument
        '''
        letters = string.ascii_letters
        key = ''.join(random.choice(letters) for i in range(len(field)))
        return f"#{key}"

    def _create_update_expressions(self, item: Dict[str, Any]) -> Tuple[str, Dict[str, Any]]:
        '''
        Creates the dynamodb update expression and expression attribute values
        used in the
        boto3.session.resource('dynamodb').Table(<table>).update_item()

        Args:
            item (Dict[str, Any]): dictionary with the new field to set

        Returns:
            (Tuple):
                * **update_expression (str)**: dynamodb update expression
                * **expression_attribute_names (Dict[str, Any])**: dynamodb
                    expression_attribute_names
                * **expression_attribute_values (Dict[str, Any])**: dynamodb
                    expression_attribute_values
        '''
        update_expression = 'SET '
        expression_attribute_names = {}
        expression_attribute_values = {}

        for field, value in item.items():
            update_expression_field = None
            if '.' in field:
                field_splits = field.split('.')
                for field_split in field_splits:
                    key = self._create_random_expression_attribute_name_key(field_split)
                    expression_attribute_names[key] = field_split
                    update_expression_field = f'{update_expression_field}.{key}' if update_expression_field else key
            else:
                key = self._create_random_expression_attribute_name_key(field)
                expression_attribute_names[key] = field
                update_expression_field = key

            expression_attribute_values[':' + field] = value
            update_expression += f'{update_expression_field}=:{field},'

        # remove the last comma
        update_expression = update_expression.rsplit(',', 1)[0]

        return (update_expression, expression_attribute_names, expression_attribute_values)

    def update_item_by_key(self, hash_key_value: str, updates: Dict[str, Any], range_key_value: str = None):
        '''
        Update an existing item in the DynamoDB table with new details using
        boto3.session.resource('dynamodb').Table(<table>).update_item()

        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Table.update_item

        Args:
            hash_key_value (str): the value of the hash key for the item we
                want to update
            updates (Dict[str, Any]): a dictionary with the fields we want to
                update
            range_key_value (str): the value of the range key for the item
                we want to retrieve, needeed only if range key defined
        '''
        LOG.debug("Update item from DynamoDB table %s for key=%s,%s with updates=%s", self.table_name, hash_key_value,
                  range_key_value, updates)

        key = self._create_key_arg(hash_key_value, range_key_value)
        (update_expression, expression_attribute_names,
         expression_attribute_values) = self._create_update_expressions(updates)

        self._boto_dynamodb_table.update_item(Key=key,
                                              UpdateExpression=update_expression,
                                              ExpressionAttributeNames=expression_attribute_names,
                                              ExpressionAttributeValues=expression_attribute_values)

    def delete_item_by_key(self, hash_key_value: str, range_key_value: str = None):
        '''
        Delete one item specifing the value of the key using
        boto3.session.resource('dynamodb').Table(<table>).delete_item(Key=<key>)

        Reference:
        https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Table.delete_item

        Args:
            hash_key_value (str): the value of the hash key for the item we
                want to delete
            range_key_value (str): the value of the range key for the item
                we want to delete, needeed only if range key defined
        '''
        LOG.debug("Delete item from DynamoDB table %s for key=%s,%s", self.table_name, hash_key_value, range_key_value)
        key = self._create_key_arg(hash_key_value, range_key_value)

        self._boto_dynamodb_table.delete_item(Key=key)
