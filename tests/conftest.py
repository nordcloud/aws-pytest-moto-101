from datetime import datetime, timedelta
import os
import random
import string
from typing import Any, Dict, List

import boto3
import pytest
from moto import mock_s3, mock_iam, mock_dynamodb2
from botocore.client import BaseClient
from boto3.resources.base import ServiceResource

from aws_package.s3_service import S3Service
from aws_package.iam_service import IamService
from aws_package.dynamodb_table import DynamoDBTable


@pytest.fixture(scope='session')
def region_name() -> str:
    return 'eu-central-1'


@pytest.fixture(scope='session')
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ['AWS_ACCESS_KEY_ID'] = 'testing'
    os.environ['AWS_SECRET_ACCESS_KEY'] = 'testing'
    os.environ['AWS_SECURITY_TOKEN'] = 'testing'
    os.environ['AWS_SESSION_TOKEN'] = 'testing'


@pytest.fixture(scope='session')
def s3_client(aws_credentials, region_name: str) -> BaseClient:
    with mock_s3():
        yield boto3.client('s3', region_name)


@pytest.fixture(scope='session')
def s3_service(s3_client: BaseClient) -> S3Service:
    return S3Service(s3_client)


@pytest.fixture(scope='session')
def bucket_name() -> str:
    return 'my-bucket'


@pytest.fixture(scope='session')
def s3_create_bucket(s3_client: BaseClient, bucket_name: str, region_name: str) -> Dict[str, Any]:
    return s3_client.create_bucket(ACL='public-read-write',
                                   Bucket=bucket_name,
                                   CreateBucketConfiguration={'LocationConstraint': region_name})


@pytest.fixture(scope='session')
def dynamodb_resource(aws_credentials, region_name: str) -> ServiceResource:
    with mock_dynamodb2():
        yield boto3.resource('dynamodb', region_name)


@pytest.fixture(scope='session')
def user_dynamodb_table(dynamodb_resource: ServiceResource) -> object:
    return dynamodb_resource.create_table(
        TableName='users',
        KeySchema=[{
            'AttributeName': 'id',
            'KeyType': 'HASH'
        }],
        AttributeDefinitions=[{
            'AttributeName': 'id',
            'AttributeType': 'S'
        }],
        BillingMode='PAY_PER_REQUEST',
    )


@pytest.fixture(scope='session')
def user_table(user_dynamodb_table: object) -> DynamoDBTable:
    return DynamoDBTable(user_dynamodb_table)


@pytest.fixture(scope='session')
def iam_client(aws_credentials, region_name: str) -> BaseClient:
    with mock_iam():
        yield boto3.client('iam', region_name)


class IamServiceFake(IamService):

    def _get_all_access_keys_by_user(self, user_name: str) -> List[Dict[str, Any]]:
        '''
        Override the method to fake the information
        '''
        return [{
            'UserName': user_name,
            'AccessKeyId': ''.join(random.choice(string.ascii_uppercase + string.digits) for i in range(20)),
            'Status': 'Active',
            'CreateDate': datetime.now() - timedelta(days=30)
        }]


@pytest.fixture(scope='session')
def iam_service(iam_client: BaseClient) -> IamService:
    return IamServiceFake(iam_client)
