from aws_package.dynamodb_table import DynamoDBTable


def test_add_item(user_table: DynamoDBTable, user_dynamodb_table: object):
    item = user_table.add_item({
        'id': '1',
        'name': 'michele',
        'surname': 'ugolini',
        'email': 'michele.ugolini@nordcloud.com'
    })
    assert item
    response = user_dynamodb_table.get_item(Key={'id': '1'}, ConsistentRead=True)
    assert response['ResponseMetadata'].get('HTTPStatusCode') == 200
    dynamodb_item = response.get('Item', {})
    assert item == dynamodb_item
