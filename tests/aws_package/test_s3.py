import json
from typing import Any, Dict

from aws_package.s3_service import S3Service


def test_s3_upload(s3_service: S3Service, bucket_name: str, s3_create_bucket: Dict[str, Any]):
    response = s3_service.upload(
        bucket_name, 'my-file.json',
        json.dumps({
            'users': [{
                'id': 1,
                'name': 'michele',
                'surname': 'ugolini',
                'email': 'michele.ugolini@nordcloud.com'
            }, {
                'id': 2,
                'name': 'name',
                'surname': 'surname',
                'email': 'name.surname@nordcloud.com'
            }]
        }))
    assert response['ResponseMetadata'].get('HTTPStatusCode') == 200
