from datetime import datetime

import pytest
from botocore.client import BaseClient

from aws_package.iam_service import IamService


@pytest.fixture(scope='session')
def user_name() -> str:
    return 'iam-user-devops'


def test_get_users_keys(iam_service: IamService, iam_client: BaseClient, user_name: str):
    response = iam_client.create_user(UserName=user_name)
    assert response['User']
    users_access_keys = iam_service.get_all_users_access_keys()
    assert isinstance(users_access_keys, list)
    devops_access_key = users_access_keys[0]['AccessKeys'][0]
    assert isinstance(devops_access_key, dict)
    assert isinstance(devops_access_key['CreateDate'], datetime)
    assert devops_access_key['Status'] == 'Active'
